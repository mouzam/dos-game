import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../DOSServices/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  offers;

  constructor(private router:Router, private dataService:DataService) {
    this.fetchDetails();
  }


  async fetchDetails(){
    let fetchOffersBodyObj = {
      store: "store_2dddbe41b7004652a2fd7addfb836847", 
      pageSize:"500",
      offset:"0"
    };
    let response = await this.dataService.fetchOffers(fetchOffersBodyObj);
    console.log(response)
    this.offers = response["payload"]["offers"];
    localStorage.setItem("data", JSON.stringify(this.offers));
   } 

  ngOnInit() {
  }

  currentItemIndex;
  currentItem;
  
  saveCurrentItem(i, offer){
    this.currentItemIndex = i;
    this.currentItem = offer;
  }

  showOffer(){
    // console.log(i)
    if(this.currentItemIndex <= 2){
      this.wheelClick(this.currentItem.mode, this.currentItem.offerId);
    }else{
        this.equationClick(this.currentItem.mode, this.currentItem.offerId);
    }
  }

  wheelClick(mode,  offerId){
    this.router.navigate(['/wheel', {mode: mode, offerId: offerId}]);
  }

  equationClick(mode, offerId){
    this.router.navigate(['/equation', {mode: mode, offerId: offerId}]);
  }
}
