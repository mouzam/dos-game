import { DataService } from './DOSServices/data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EquationsComponent } from './equations/equations.component';
import { WheelComponent } from './wheel/wheel.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { HomeComponent } from './home/home.component';
import { HttpModule } from '@angular/http';
import { StorageService } from './DOSServices/storage.service';
@NgModule({
  declarations: [
    AppComponent,
    EquationsComponent,
    WheelComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    // HttpClientModule,
    AppRoutingModule,
    BsDropdownModule.forRoot()
  ],
  providers: [DataService, StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
