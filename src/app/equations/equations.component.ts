import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, SecurityContext, ViewEncapsulation } from '@angular/core';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

declare let Phaser: any
// declare let String.prototype.isNumeric;
// declare let Array.prototype.clean;
// declare let String.isNumeric;
interface String {
  isNumeric();
}


// String.prototype.isNumeric = function () {
//   return !isNaN(parseFloat(this)) && isFinite(this);
// }


/**
This function is used to clean the input from spaces.
*/
// Array.prototype.clean = function () {
//   for (var i = 0; i < this.length; i++) {
//     if (this[i] === "") {
//       this.splice(i, 1);
//     }
//   }
//   return this;
// }

class MathSolver {
  infixToPostfix(infix) {
    var outputQueue = [];
    var operatorStack = [];
    var operators = {
      "/": {
        precedence: 3,
        associativity: "Left"
      },
      "*": {
        precedence: 3,
        associativity: "Left"
      },
      "+": {
        precedence: 2,
        associativity: "Left"
      },
      "-": {
        precedence: 2,
        associativity: "Left"
      }
    }
    infix = infix.replace(/\s+/g, "");
    infix = infix.split(/([\+\-\*\/\^\(\)])/).clean();
    // console.log(infix)
    for (var i = 0; i < infix.length; i++) {
      var token = infix[i];
      if (token.isNumeric()) {
        outputQueue.push(token + "");
      } else if ("^*/+-".indexOf(token) !== -1) {
        var o1 = token;
        var o2 = operatorStack[operatorStack.length - 1];
        while ("^*/+-".indexOf(o2) !== -1 && ((operators[o1].associativity === "Left" && operators[o1].precedence <= operators[o2].precedence) || (operators[o1].associativity === "Right" && operators[o1].precedence < operators[o2].precedence))) {
          outputQueue.push(operatorStack.pop() + "");
          o2 = operatorStack[operatorStack.length - 1];
        }
        operatorStack.push(o1);
      } else if (token === "(") {
        operatorStack.push(token);
      } else if (token === ")") {
        while (operatorStack[operatorStack.length - 1] !== "(") {
          outputQueue.push(operatorStack.pop() + "");
        }
        operatorStack.pop();
      }
    }
    while (operatorStack.length > 0) {
      outputQueue.push(operatorStack.pop() + "");
    }
    return outputQueue;
  }
}

class Node {
  data;
  left;
  right;
}

class Tree {
  root;

  addNode(n) {

    var currentNode;
    if (this.root == undefined) {
      this.root = n;
    } else {
      currentNode = this.findNode(this.root);
      if (currentNode.right == undefined) {
        currentNode.right = n;
      } else if (currentNode.left == undefined) {
        currentNode.left = n;
      }
    }
  }

  findNode(r) {
    var finalNode;
    if (this.isNumeric(r.data)) {
      return;
    }

    if (r.right == undefined) {
      return r;
    } else if (r.right != undefined && !this.isNumeric(r.right)) {
      finalNode = this.findNode(r.right);
      if (finalNode != undefined)
        return finalNode;
    }

    if (r.left == undefined) {
      return r;
    } else if (r.left != undefined && !this.isNumeric(r.left)) {
      finalNode = this.findNode(r.left);
    }
    return finalNode;
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  print() {
    console.log("--------Tree------------")
    console.log("root:");
    console.log(this.root);
    console.log("----------------")
    console.log();
  }

  evaluate() {
    console.log("Evaluate working ")
    return this.evaluateExpressionTree(this.root);
  }

  evaluateExpressionTree(root) {

    //empty tree 
    if (root == undefined) {
      return 0
    }

    //leaf node 
    if (root.left == undefined && root.right == undefined) {
      return parseInt(root.data)
    }

    //evaluate left tree 
    var left_sum = this.evaluateExpressionTree(root.left)

    //evaluate right tree 
    var right_sum = this.evaluateExpressionTree(root.right)

    //check which operation to apply 
    if (root.data == '+')
      return left_sum + right_sum
    else if (root.data == '-')
      return left_sum - right_sum
    else if (root.data == '*')
      return left_sum * right_sum
    else
      return left_sum / right_sum
  }
}




class EquationPhaser extends Phaser.Scene {
  // constructor
  constructor() {
    super("PlayGame");
  }

  // method to be executed when the scene preloads
  preload() {
  }

  // method to be executed once the scene has been created
  create() {
    //                    text = this.add.text(widthC/2, 600-50, '', { fill: '#0f0' })
    //                    this.drawKeyPad();
  }


  createBtn() {

  }



  drawButton(startPosX, startPosY, width, buttonSymbol) {
    var rect = this.add.rectangle(startPosX, startPosY, width, width, 0x888888, 1)
    var text = this.add.text(startPosX, startPosY, buttonSymbol, { fill: '#fff' })
    text.setFontSize('36px');
    text.setOrigin(0.5)
    rect.setInteractive();
    return rect;
  }
}



// // pure javascript to scale the game
// function resize() {
//     var canvas = document.querySelector("canvas");
//     var windowWidth = window.innerWidth;
//     var windowHeight = window.innerHeight;
//     var windowRatio = windowWidth / windowHeight;
//     var gameRatio = game.config.width / game.config.height;
//     if(windowRatio < gameRatio){
//         canvas.style.width = windowWidth + "px";
//         canvas.style.height = (windowWidth / gameRatio) + "px";
//     }
//     else{
//         canvas.style.width = (windowHeight * gameRatio) + "px";
//         canvas.style.height = windowHeight + "px";
//     }
// }

@Component({
  selector: 'app-equations',
  templateUrl: './equations.component.html',
  styleUrls: ['./equations.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class EquationsComponent implements OnInit {
  game;
  parentDiv;
  currentMode;
  htmlContent = '';


  modes = {
    VERY_EASY: {
      noOfOperators: 2,
      operators: ['+', '-', '*']
    },
    EASY: {
      noOfOperators: 4,
      operators: ['+', '-', '*']
    },
    MEDIUM: {
      noOfOperators: 6,
      operators: ['+', '-', '*', '/']
    },
    HARD: {
      noOfOperators: 8,
      operators: ['+', '-', '*', '/']
    },
    VERY_HARD: {
      noOfOperators: 10,
      operators: ['+', '-', '*', '/']
    }
  }

  resetEquation() {
    this.EQUATION.CONSTANTS = [];
    this.EQUATION.VARIABLES = [];
    this.EQUATION.OPERATORS = [];
  }

  setMode(mode) {
    console.log(mode)
    this.removeEquation();
    this.resetEquation();
    if (mode == 'easy') {
      this.currentMode = this.modes.EASY
    } else if (mode == 'veryEasy') {
      this.currentMode = this.modes.VERY_EASY
    } else if (mode == 'medium') {
      this.currentMode = this.modes.MEDIUM
    } else if (mode == 'hard') {
      this.currentMode = this.modes.HARD
    } else if (mode == 'veryHard') {
      this.currentMode = this.modes.VERY_HARD
    }else{
      console.log("no mode found")
    }
    this.generateEquation(this.currentMode);
  }

  removeEquation() {
    const myNode = document.getElementById("equationDiv");
    if (undefined == myNode)
      return;
    while (myNode.firstChild) {
      myNode.removeChild(myNode.firstChild);
    }
  }
  gameOptions = {
    // slices (prizes) placed in the wheel
    slices: 8,
    // slice colors
    sliceColors: [0xff0000, 0x00ff00, 0x00d0ff, 0xff00ff, 0xaa00aa, 0xffff00, 0x8dd888, 0x0000ff],
    // prize names
    slicePrizes: ["RED", "GREEN", "BLUE", "WHITE", "CYAN", "YELLOW", "GREY", "PURPLE"],
    // wheel radius, in pixels
    wheelRadius: 160,
    totalTurns: 3
  }

  widthC = 1000;
  heightC = 600;
  text;
  buttons = [
    '+', '-', '*', '/', '0', '1', '2', '3', '4',
    '5', '6', '7', '8', '9', 'C'
  ]

  EQUATION = {
    CONSTANTS_ID: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'],
    VARIABLES_ID: ['i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'],
    CONSTANTS: [],
    VARIABLES: [],
    OPERATORS: []
  }
  xRandomNumber;
  yRandomNumber;
  zRandomNumber;
  aRandomNumber;
  bRandomNumber;
  cRandomNumber;
  ans;
  userAns;

  constructor(@Inject(DOCUMENT) private document: Document, private sanitizer: DomSanitizer, private route:ActivatedRoute) {
   }

  isNumeric(number) {
    return !isNaN(parseFloat(number)) && isFinite(number);
  }

  /**
  This function is used to clean the input from spaces.
  */
  clean(input) {
    for (var i = 0; i < input.length; i++) {
      if (this[i] === "") {
        input.splice(i, 1);
      }
    }
    return this;
  }





  ngOnInit() {
    var gameConfig = {
      type: Phaser.CANVAS,
      width: 1000,
      height: 600,
      backgroundColor: 0xffffff,
      scene: [EquationPhaser]
    };
    // this.game = new Phaser.Game(gameConfig);
    this.parentDiv = this.document.getElementById('equationDiv');
    var aElement = this.document.createElement("p");
    aElement.setAttribute("id", "id");
    aElement.innerHTML = "value"
    aElement.className = "d-inline"
    this.parentDiv.appendChild(aElement)
    console.log(this.parentDiv)
    // this.text = this.add.text(this.widthC/2, 600-50,'i am text', {fill: '#0f0'})
    this.parentDiv = document.getElementById('equationDiv');
    // this.setMode('veryEasy')
    var mode = this.route.snapshot.paramMap.get('mode')
    this.setMode(mode)
 
  }

  generateEquation(mode) {
    this.answered = false;
    this.htmlContent = '';
    var aElement;
    var randomNo;
    for (var i = 0; i < (mode.noOfOperators) + 1; ++i) {
      aElement = document.createElement("p");
      aElement.innerHTML = "("
      aElement.className += "d-inline"
      aElement.className += " constants"
      aElement.className += " p"
      this.parentDiv.appendChild(aElement)
      this.htmlContent += aElement.outerHTML

      randomNo = Math.floor(Math.random() * 20) + 2;
      this.EQUATION.CONSTANTS.push(randomNo);
      this.createP(this.EQUATION.CONSTANTS_ID[i], randomNo);

      aElement = document.createElement("p");
      aElement.innerHTML = "&#215;"
      aElement.className += "d-inline"
      aElement.className += " constants"
      aElement.className += " p"
      this.parentDiv.appendChild(aElement)
      this.htmlContent += aElement.outerHTML


      randomNo = Math.floor(Math.random() * 20) + 2;
      this.EQUATION.VARIABLES.push(randomNo);
      this.createInput(this.EQUATION.VARIABLES_ID[i]);
      if (i != (mode.noOfOperators)) {
        aElement = document.createElement("p");
        var operator = this.currentMode.operators[Math.floor(Math.random() * this.currentMode.operators.length)]
        this.EQUATION.OPERATORS.push(operator)
        aElement.innerHTML = operator;
        aElement.className = "d-inline"
        aElement.className += " constants"
        aElement.className += " p"
        this.parentDiv.appendChild(aElement)
        this.htmlContent += aElement.outerHTML;
      }
    }
    var ans = 0;
    var equationLeftSideStr = '';
    for (var i = 0; i < mode.noOfOperators + 1; ++i) {
      if (i == 0) {
        ans += this.EQUATION.CONSTANTS[i] * this.EQUATION.VARIABLES[i];
        equationLeftSideStr += "(" + this.EQUATION.CONSTANTS[i] + '*' + this.EQUATION.VARIABLES[i] + ")";
      }
      else {
        let operator = this.EQUATION.OPERATORS[i - 1];
        if (operator == "+") {
          ans += this.EQUATION.CONSTANTS[i] * this.EQUATION.VARIABLES[i];
          equationLeftSideStr += '+(' + this.EQUATION.CONSTANTS[i] + '*' + this.EQUATION.VARIABLES[i] + ')';
        } else if (operator == "*") {
          ans *= this.EQUATION.CONSTANTS[i] * this.EQUATION.VARIABLES[i];
          equationLeftSideStr += '*(' + this.EQUATION.CONSTANTS[i] + '*' + this.EQUATION.VARIABLES[i] + ')';
        } else if (operator == "/") {
          ans /= this.EQUATION.CONSTANTS[i] * this.EQUATION.VARIABLES[i];
          equationLeftSideStr += '/(' + this.EQUATION.CONSTANTS[i] + '*' + this.EQUATION.VARIABLES[i] + ')';
        } else if (operator == "-") {
          ans -= this.EQUATION.CONSTANTS[i] * this.EQUATION.VARIABLES[i];
          equationLeftSideStr += '-(' + this.EQUATION.CONSTANTS[i] + '*' + this.EQUATION.VARIABLES[i] + ')';
        }
      }
    }

    var ms = new MathSolver();
    var postFix = ms.infixToPostfix(equationLeftSideStr);
    var tree = new Tree();
    var node;
    for (var i = postFix.length - 1; i >= 0; --i) {
      node = new Node();
      node.data = postFix[i];
      tree.addNode(node);
    }
    // tree.print()
    console.log(tree.evaluate())
    console.log(equationLeftSideStr);

    this.ans = tree.evaluate();
    
    aElement = document.createElement("p");
    aElement.innerHTML = "="
    aElement.className = "d-inline"
    aElement.className += " constants"
    aElement.className += " p"
    // this.parentDiv.appendChild(aElement)
    // console.log(aElement)
    this.htmlContent += aElement.outerHTML;
    this.createP("ans", this.ans);


    // document.getElementById('ans').innerHTML= this.ans
    console.log(this.EQUATION.VARIABLES)
    // console.log(this.htmlContent) 
  }
  
  createP(id, value) {
    var aElement;
    aElement = document.createElement("p");
    aElement.setAttribute("id", id);
    aElement.innerHTML = value
    aElement.className += "d-inline"
    aElement.className += " constants"
    aElement.className += " p"
    this.parentDiv.appendChild(aElement)
    // console.log(aElement.outerHTML)
    this.htmlContent += aElement.outerHTML;
  }

  getHtml() {
    // console.log(this.htmlContent)
    return this.sanitizer.bypassSecurityTrustHtml(this.htmlContent);
  }


  createInput(id) {
    var bElement;
    bElement = document.createElement("input");
    bElement.setAttribute("id", id);
    bElement.setAttribute("size", "2");
    bElement.innerHTML = id
    bElement.className = "d-inline"
    bElement.className += " p"
    this.parentDiv.appendChild(bElement)
    // console.log(bElement.outerHTML)
    this.htmlContent += bElement.outerHTML;
    var aElement;
    aElement = document.createElement("p");
    aElement.innerHTML = ")"
    aElement.className += "d-inline"
    aElement.className += " constants"
    aElement.className += " p"
    this.parentDiv.appendChild(aElement);
    this.htmlContent += aElement.outerHTML
  }

  submit() {
    var userVariables = [];
    for (var i = 0; i < this.EQUATION.VARIABLES.length; ++i) {
      var id = this.EQUATION.VARIABLES_ID[i];
      var value;
      value = this.document.getElementById(id);
      // console.log(value.value)
      userVariables.push(value.value);
    }
    var ans = 0;
    // console.log(userVariables)
    var equationLeftSideStr = '';
    for (var i = 0; i < this.EQUATION.CONSTANTS.length; ++i) {
      // console.log("in loop")
      if (i == 0) {
        ans += this.EQUATION.CONSTANTS[i] * userVariables[i];
        equationLeftSideStr += "(" + this.EQUATION.CONSTANTS[i] + '*' + userVariables[i] + ")";
      }
      else {
        var operator = this.EQUATION.OPERATORS[i - 1];
        if (operator == "+") {
          ans += this.EQUATION.CONSTANTS[i] * userVariables[i];
          equationLeftSideStr += '+(' + this.EQUATION.CONSTANTS[i] + '*' + userVariables[i] + ')';
        } else if (operator == "*") {
          ans *= this.EQUATION.CONSTANTS[i] * userVariables[i];
          equationLeftSideStr += '*(' + this.EQUATION.CONSTANTS[i] + '*' + userVariables[i] + ')';
        } else if (operator == "/") {
          ans /= this.EQUATION.CONSTANTS[i] * userVariables[i];
          equationLeftSideStr += '/(' + this.EQUATION.CONSTANTS[i] + '*' + userVariables[i] + ')';
        } else if (operator == "-") {
          ans -= this.EQUATION.CONSTANTS[i] * userVariables[i];
          equationLeftSideStr += '-(' + this.EQUATION.CONSTANTS[i] + '*' + userVariables[i] + ')';
        }
      }
    }
    var ms = new MathSolver();
    var postFix = ms.infixToPostfix(equationLeftSideStr);
    var tree = new Tree();
    var node;
    for (var i = postFix.length - 1; i >= 0; --i) {
      node = new Node();
      node.data = postFix[i];
      tree.addNode(node);
    }
    ans = tree.evaluate();
    this.userAns = ans;
    this.answered = true;
    if (ans == this.ans) {
      console.log("Answer is correct ans:" + ans + ": this.ans: " + this.ans);
      this.isCorrect = true;
      // this.text.text = "Answer is correct ans:" + ans + ": this.ans: " + this.ans;   
    } else {
      this.isCorrect = false;
      console.log("Answer is wrong ans:" + ans + ": this.ans: " + this.ans)
      // this.text.text = "Answer is wrong ans:" + ans + ": this.ans: " + this.ans;   
    }
  }
  answered = false;
  isCorrect = false;
}
