import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { DOSConstants } from '../DOSUtils/dos-constants';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpService: HttpService) { }

  async fetchOffers(fetchBodyObj){
    let offersUrl = DOSConstants.baseUrl + DOSConstants.fetchOffers;
    let apiResponse = await this.httpService.post(offersUrl, fetchBodyObj).toPromise();
    return (apiResponse.json())
  }

  async createSession(sessionBodyobj){
    let createSessionUrl = DOSConstants.baseUrl + DOSConstants.createSession;
    let apiResponse = await this.httpService.post(createSessionUrl, sessionBodyobj).toPromise();
    return (apiResponse.json())
  }

  async closeSession(closeSessionBodyObj){
    let closeSessionUrl = DOSConstants.baseUrl + DOSConstants.closeSession;
    let apiResponse = await this.httpService.post(closeSessionUrl ,closeSessionBodyObj).toPromise();
    return (apiResponse.json())
  }
}
