import { Injectable } from '@angular/core';
import { Http } from '@angular/http';



@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http:Http) { }

  post(url, obj){
    return this.http.post(url, obj)    
  }

  get(url){
   return this.http.get(url)
  }
}