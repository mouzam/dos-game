import { HomeComponent } from './home/home.component';
import { WheelComponent } from './wheel/wheel.component';
import { EquationsComponent } from './equations/equations.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    data: {
        breadcrumb: '',
    },
    component: HomeComponent,
  },
  {
    path: 'wheel',
    data: {
        breadcrumb: 'wheel',
    },
    component: WheelComponent,
  },
  {
    path: 'equation',
    data: {
        breadcrumb: 'equation',
    },
    component: EquationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
