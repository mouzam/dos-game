export class BBSessions {

    constructor() { }

    public static createLocal(key, object){
        // console.log("create local called" + object)
        localStorage.setItem(key, object);
    }
    public static getLocal(key){
        // console.log("get local called" + key)
        let localData =  localStorage.getItem(key);

        return JSON.parse(localData);
    }

    public static deleteLocal(key){
        localStorage.removeItem(key)
    }
}