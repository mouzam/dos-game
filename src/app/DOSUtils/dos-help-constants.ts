export class DOSHelpConstants {
    public static veryEasy = '"veryEasy"';
    public static easy = '"easy"';
    public static medium = '"medium"';
    public static hard = '"hard"';
    public static veryHard = '"veryHard"'
}