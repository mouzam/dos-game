export class DOSConstants {

  constructor() { }

  public static baseUrl = "http://3.1.104.232/";
  public static fetchOffers = "dos/api/v1/fetch/offers";
  public static createSession = "dos/api/v1/session/create";
  public static closeSession = "dos/api/v1/session/close";

  /**set showlog to true during debugging else false */
  public static showlog= true;
}