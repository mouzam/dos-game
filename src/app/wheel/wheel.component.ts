import { HttpService } from './../DOSServices/http.service';
import { StorageService } from './../DOSServices/storage.service';
import { DOSHelpConstants } from './../DOSUtils/dos-help-constants';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Router, ActivatedRoute } from '@angular/router';
import { DOSConstants } from '../DOSUtils/dos-constants';
import { DataService } from '../DOSServices/data.service';
declare let Phaser: any;
@Component({
  selector: 'app-wheel',
  templateUrl: './wheel.component.html',
  styleUrls: ['./wheel.component.scss']
})

export class WheelComponent implements OnInit, OnDestroy {
  game;

  constructor(private route: ActivatedRoute, private storageService: StorageService, private dataService: DataService) {
    console.log(this.storageService)
    console.log("constructor");

    let wheelPhaserObj = new WheelPhaser(dataService);

    var gameConfig = {
      type: Phaser.CANVAS,
      width: 1200,
      height: 600,
      backgroundColor: 0xffffff,
      parent: 'parent',
      scene: wheelPhaserObj
    };
    this.game = new Phaser.Game(gameConfig);
    console.log(this.game.scene.scenes)
    this.resize();
  }

  ngOnInit() {
    var mode = this.route.snapshot.paramMap.get('mode')
    console.log("mode =", mode)
    this.storageService.setItem("mode", mode);

    var offerId = this.route.snapshot.paramMap.get("offerId")
    this.storageService.setItem("offerId", offerId);
  }

  ngAfterViewInit() {
    // console.log("ngOnAfterInit");
    // console.log(this.game.scene.scenes)
  }


  ngOnDestroy() {
    var canvas = document.getElementsByTagName("canvas")[0];
    var body = document.getElementsByTagName("body");
    // console.log(body);
    body[0].removeChild(canvas);
    this.game.destroy();
  }

  veryEasyDD() {
    this.game.scene.scenes[0].setVeryEasyMode();
  }

  easyDD() {
    this.game.scene.scenes[0].setEasyMode();
    // console.log("easy: Wheel Componenet");
  }

  mediumDD() {
    this.game.scene.scenes[0].setMediumMode();
    // console.log("medium: Wheel Component")
  }

  hardDD() {
    this.game.scene.scenes[0].setHardMode();
    // console.log("hard: Wheel Component")
  }

  veryHardDD() {
    this.game.scene.scenes[0].setVeryHardMode();
    // console.log("hard: Wheel Component")
    // console.log("very hard component")
  }

  resize() {
    var canvas = document.querySelector("canvas");
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    var windowRatio = windowWidth / windowHeight;
    var gameRatio = this.game.config.width / this.game.config.height;
    if (windowRatio < gameRatio) {
      canvas.style.width = windowWidth + "px";
      canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else {
      canvas.style.width = (windowHeight * gameRatio) + "px";
      canvas.style.height = windowHeight + "px";
    }
  }
}


class WheelPhaser extends Phaser.Scene {
  canSpin;
  startStopButton;
  timerText;
  wheel;
  timer;
  game;
  currentTurn = 0;
  timmerValue = 30;
  speed;
  initTimerValue = 0;
  initTimer;
  direction; //+ve represent clockwise direction -ve for anti clockwise
  sessionCreated: boolean = false;

  MODES = {
    VERY_EASY: {
      turns: 2,
      timer: 20,
      speed: 5,
      slices: 15
    },
    EASY: {
      turns: 3,
      timer: 20,
      speed: 6,
      slices: 7
    },
    MEDIUM: {
      turns: 5,
      timer: 15,
      speed: 8,
      slices: 9
    },
    HARD: {
      turns: 7,
      timer: 10,
      speed: 10,
      slices: 11
    },
    VERY_HARD: {
      turns: 6,
      timer: 20,
      speed: 12,
      slices: 10
    }
  }
  currentMode;

  setVeryEasyMode() {
    this.currentMode = this.MODES.VERY_EASY;
    this.drawWheel();
    this.canSpin = false;
    var pin = this.add.image(0, 0, 'pin');
    pin.width = 50;
    pin.height = 50;
    pin.x = 1200 / 2;
    pin.y = 600 / 5.6;
    pin.angle = 180
  }

  setEasyMode() {
    this.currentMode = this.MODES.EASY;
    this.drawWheel();
    this.canSpin = false;
    
    var pin = this.add.image(0, 0, 'pin');
    pin.width = 50;
    pin.height = 50;
    pin.x = 1200 / 2;
    pin.y = 600 / 5.6;
    pin.angle = 180
  }

  setMediumMode() {
    this.currentMode = this.MODES.MEDIUM;
    this.drawWheel();
    this.canSpin = false;
    var pin = this.add.image(0, 0, 'pin');
    pin.width = 50;
    pin.height = 50;
    pin.x = 1200 / 2;
    pin.y = 600 / 5.6;
    pin.angle = 180
  }

  setHardMode() {
    this.currentMode = this.MODES.HARD;
    this.drawWheel();
    this.canSpin = false;
    var pin = this.add.image(0, 0, 'pin');
    pin.width = 50;
    pin.height = 50;
    pin.x = 1200 / 2;
    pin.y = 600 / 5.6;
    pin.angle = 180
  }

  setVeryHardMode() {
    this.currentMode = this.MODES.VERY_HARD;
    this.drawWheel();
    this.canSpin = false;
    var pin = this.add.image(0, 0, 'pin');
    pin.width = 50;
    pin.height = 50;
    pin.x = 1200 / 2;
    pin.y = 600 / 5.6;
    pin.angle = 180
  }

  gameOptions = {
    slices: 8,
    sliceColors: [0xff0000, 0x00ff00, 0x00d0ff, 0xff00ff, 0xaa00aa, 0xffff00, 0x8dd888, 0x0000ff,
      0xff00dd, 0xddff00, 0xffd0ff, 0xffaaff, 0xaaaaaa, 0xffffcc, 0x8dd444, 0x4400ff],
    slicePrizes: ["RED", "GREEN", "BLUE", "WHITE", "CYAN", "YELLOW", "GREY", "PURPLE"],
    wheelRadius: 160,
    totalTurns: 3
  }

  gamePallets = {
    width: 20,
    height: 20,
    x: 50,
    y: 50
  }

  isSame = false;
  previousColor;
  resultText;

  constructor(private dataService: DataService) {
    super("PlayGame");
    // console.log(this.dataService)
  }

  easyDD() {
    this.speed = 0.6;
    // console.log("easy");
  }

  mediumDD() {
    // console.log("medium")
    this.speed = 1.5
  }

  hardDD() {
    // console.log("hard")
    this.speed = 3
  }

  // method to be executed when the scene preloads
  preload() {
    this.load.image('pin', 'assets/pin.png')
  }

  // method to be executed once the scene has been created
  create() {

    // this.drawInitialPallets();

    // the game has just started = we can spin the wheel

    this.resultText = this.add.text(1200 / 2 + 200, 600 / 2, '', { fill: '#0f0' })
    var rect = this.add.rectangle(1200 / 2, 600 - 50, 130, 50, 0x888888, 1)
    this.startStopButton = this.add.text(1200 / 2, 600 - 50, "SPIN", { fill: '#fff' })
    this.startStopButton.setFontSize('36px');
    this.startStopButton.setOrigin(0.5)
    rect.setInteractive();


    // this.startStopButton = this.add.text(1200 / 2, 600 - 50, 'SPIN', { fill: '#0f0' })
    this.startStopButton.setInteractive();
    // this.startStopButton.setOrigin(0.5);

    this.startStopButton.on('pointerdown', async () => {
      console.log(this.currentTurn);

      if (this.sessionCreated == false) {
        this.sessionCreated = true;
        let offerId = JSON.parse(localStorage.getItem("offerId"));
        let createSessionBody = {
          store: "store_0e6d8f3a00a74a0fbbebf74185d50611",
          branch: "branch_9f8c30464caf4db3ac65e150064e1c01",
          offer: offerId
        }
        console.log(this.dataService)
        let apiResponse = await this.dataService.createSession(createSessionBody);
        console.log(apiResponse)
        localStorage.setItem("sessionId", apiResponse["payload"]["sessionId"]);
      }

      if (this.currentTurn == -1) {
        this.currentTurn++;
        this.startStopButton.text = "SPIN";
        return;
      }
      let rand = Math.random()
      
      if (rand > 0.5) {
        this.direction = -1;
      } else {
        this.direction = 1;
      }

      if (this.currentTurn == this.currentMode.turns) {
        this.reset();
        this.resultText.text = ""
        if (this.canSpin) {
          this.startStopButton.text = "STOP";
        } else {
          this.startStopButton.text = "SPIN";
        }
        return;
      }

      this.spinWheel();
      if (this.canSpin) {
        this.startStopButton.text = "STOP";
      } else {
        this.startStopButton.text = "SPIN";
      }
      // console.log(this.currentTurn + ":" + this.currentMode.turns)
      if (this.currentTurn == this.currentMode.turns) {
        this.startStopButton.text = "RESET";

        if (this.sessionCreated == true) {
          this.sessionCreated = false;
          let sessionId = localStorage.getItem("sessionId");
          let closeSessionObj = {
            sessionId: sessionId,
            sessionDuration: 20,
            hasWon: true
          }
          let apiResponse = await this.dataService.closeSession(closeSessionObj);
        }
        if (this.isSame) {
          console.log('Same =', this.isSame);
          this.resultText.text = "You win";
        } else {
          this.resultText.text = "you lose";
        }
      }
    });
    this.timerText = this.add.text(600 - 100, 80, '', { fill: '#000000' })
    this.setMode();


  }

  setMode() {
    let mode = localStorage.getItem("mode");
    // console.log(mode == DOSHelpConstants.easy )
    if (mode == DOSHelpConstants.veryEasy) {
      this.setVeryEasyMode();
    }
    else if (mode == DOSHelpConstants.easy) {
      this.setEasyMode()
    }
    else if (mode == DOSHelpConstants.medium) {
      this.setMediumMode();
    } else if (mode == DOSHelpConstants.hard) {
      // console.log("hard")
      this.setHardMode();
    } else if (mode == DOSHelpConstants.veryHard) {
      // console.log("very hard")
      this.setVeryHardMode();
    }
    else {
      console.log("no mode found")
    }
  }





  reset() {
    this.currentTurn = -1;
    this.clearPallets();
  }


  startTimer() {
    this.initTimerValue = 5;
    this.initTimer = this.time.addEvent({
      delay: 1000,
      callback: this.startInitTimer,
      callbackScope: this,
      loop: true
    });
  }

  startInitTimer() {
    // console.log(this.initTimerValue)
    if (this.initTimerValue == 0) {
      this.timmerValue = this.currentMode.timer
      this.showTimer();
      this.initTimerValue--;
      this.initTimer.remove()
    } else if (this.initTimerValue > 0) {
      this.initTimerValue--;
    }
  }

  showTimer() {
    // console.log(this.timmerValue)
    this.timer = this.time.addEvent({
      delay: 1000,
      callback: this.updateCounter,
      callbackScope: this,
      loop: true
    });
  }

  updateCounter() {
    if (this.canSpin) {
      this.timmerValue = this.timmerValue - 1;
      // console.log("timer value" + this.timmerValue);
      if (this.timmerValue > 0) {
        this.timerText.text = this.timmerValue;
      }
    } else if (!this.canSpin) {
      // console.log("twice")
      this.timerText.text = ' ';
      this.stopTimer();
      this.timer.remove();
      this.timmerValue = this.currentMode.timmerValue;
      // if(this.canSpin){
      //     this.startStopButton.text = "Stop";
      // }else{
      //     this.startStopButton.text = "Start";
      // }
      // this.createPallets();
      // this.currentTurn++;
      //   this.timer.loop = false;
    }
  }

  stopTimer() {
    this.timer.loop = false;
  }

  drawInitialPallets() {
    for (var i = 0; i < this.currentMode.turns; ++i) {
      this.add.rectangle(this.gamePallets.x + ((i * 2) * this.gamePallets.width), this.gamePallets.y, this.gamePallets.width, this.gamePallets.height, 0x888888, 1)
    }
  }

  clearPallets() {
    for (var i = 0; i < this.currentMode.turns; ++i) {
      this.add.rectangle(this.gamePallets.x + ((i * 2) * this.gamePallets.width), this.gamePallets.y, this.gamePallets.width, this.gamePallets.height, 0xffffff, 1)
    }
  }

  createPallets() {
    if (this.currentTurn < this.currentMode.turns) {
      var angle = this.wheel.angle;
      if (angle < 0)
        angle *= -1;
      else
        angle = 360 - angle;
      var divisor = 360 / (this.currentMode.slices);
      angle = angle / divisor
      // console.log(parseInt(angle + 1))
      // console.log(this.wheel.angle)
      var currentColor = this.gameOptions.sliceColors[parseInt(angle)];
      if (this.currentTurn > 1 && this.previousColor == currentColor) {
        this.isSame = true;
      } else {
        this.isSame = false;
      }
      this.previousColor = currentColor;
      this.add.rectangle(this.gamePallets.x + ((this.currentTurn * 2) * this.gamePallets.width), this.gamePallets.y, this.gamePallets.width, this.gamePallets.height, currentColor, 1)
    }
  }



  drawWheel() {
    // calculating the degrees of each slice
    var sliceDegrees = 360 / this.currentMode.slices;

    // making a graphic object without adding it to the game
    var graphics = this.make.graphics({
      x: 0,
      y: 0,
      add: false
    });

    // looping through each slice
    for (var i = 0; i < this.currentMode.slices; i++) {

      // setting graphics fill style
      graphics.fillStyle(this.gameOptions.sliceColors[i], 1);

      // drawing the slice
      graphics.slice(this.gameOptions.wheelRadius, this.gameOptions.wheelRadius, this.gameOptions.wheelRadius, Phaser.Math.DegToRad(270 + i * sliceDegrees), Phaser.Math.DegToRad(270 + (i + 1) * sliceDegrees), false);

      // filling the slice
      graphics.fillPath();
    }

    // generate a texture called "wheel" from graphics data
    graphics.generateTexture("wheel", this.gameOptions.wheelRadius * 2, this.gameOptions.wheelRadius * 2);

    // creating a sprite with wheel image as if it was a preloaded image
    this.wheel = this.add.sprite(this.game.config.width / 2, this.game.config.height / 2, "wheel");
    this.wheel.angle = Math.floor(Math.random() * 360);

    //                // adding the pin in the middle of the canvas
    //        this.pin = this.add.sprite(game.config.width / 2, game.config.height / 2, "pin");
  }

  update() {
    if (this.canSpin) {
      // console.log(this.currentMode)
      console.log(this.direction)
      this.wheel.angle += this.currentMode.speed * this.direction;
    }
  }

  // function to spin the wheel
  spinWheel() {
    this.canSpin = !this.canSpin;
    if (this.currentTurn == 0) {
      this.drawInitialPallets();
    }

    if (this.canSpin == false) {
      this.createPallets();
      this.currentTurn++;
    }
    else if (this.canSpin) {
      this.startTimer();
    }
  }
}